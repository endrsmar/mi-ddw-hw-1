import scrapy
import json
 
class CZCSpider(scrapy.spiders.SitemapSpider):
    name = 'czc_spider'
    custom_settings = {
        'COMPRESSION_ENABLED': False,
        'USER_AGENT': 'MartinEndrstBot (endrsmar@fit.cvut.cz)',
        'CONCURRENT_REQUESTS': 1,
        'DOWNLOAD_DELAY': 1,
        'FEED_EXPORT_ENCODING': 'utf-8'
    }
    sitemap_urls = ['https://www.czc.cz/sitemap1.xml.gz']
    sitemap_rules = [
        ('/produkt$', 'parse_product'),
    ]
 
    def parse_product(self, response):
        product_info = {
            'productNumber': response.xpath('//span[contains(@class,"our-code")]/b[contains(@class,"data-code")]/text()').extract_first().strip().replace("&nbsp;", " "),
            'title': response.xpath('//h1[@title]/text()').extract_first().strip().replace("&nbsp;", " "),
            'parameters': {}
        }
        desc = response.xpath('//p[contains(@class,"pd-shortdesc")]/text()').extract_first()
        if desc:
            product_info['description'] = desc.strip().replace("&nbsp;", " ")
        price = response.xpath('//span[contains(@class,"price")]/text()').extract_first()
        if price:
            product_info['price'] = price.strip().replace("&nbsp;", " ")
        for parameter_item in response.xpath('//div[contains(@class,"pd-parameter-item")]/p'):
            name = parameter_item.xpath('span/text()').extract_first()
            value = parameter_item.xpath('strong/text()').extract_first()
            if name and value:
                product_info['parameters'][name.strip().replace("&nbsp;", " ")] = value.strip().replace("&nbsp;", " ")
        yield product_info


